FROM node:lts-alpine
#LABEL maintainer "Ray Ch<i@iraycd.com>"
# Set the working directory
#RUN mkdir -p /app/public
#COPY public/ /app/public/

WORKDIR /app

# Copy project specification and dependencies lock files
COPY package.json yarn.lock ./

RUN yarn --only=production

# Copy app sources
COPY . .
# Run linters and tests
#RUN yarn lint && yarn test

# Expose application port
EXPOSE 5000
# In production environment
ENV NODE_ENV production
RUN yarn build
# Run
CMD ["yarn", "start"]
