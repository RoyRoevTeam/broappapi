import mongoose from 'mongoose'
// import { userSchema } from './User';

const imageSchema = new mongoose.Schema({
  title: String,
  // createBy: userSchema,
  link: String
})

const Image = mongoose.model('Image', imageSchema)

export { imageSchema }

export default Image
