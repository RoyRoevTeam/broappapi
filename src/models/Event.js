import mongoose from 'mongoose'
import { imageSchema } from './Image'

const eventsSchema = new mongoose.Schema({
  title: String,
  date: Date,
  description: String,
  category: String,
  address: String,
  fullDay: Boolean,
  personsCount: String,
  location: {
    name: String
  },
  meta: {
    people: Number,
    likes: Number
  },
  image: imageSchema,
  createdBy: String
})

const Event = mongoose.model('Event', eventsSchema)

export default Event
