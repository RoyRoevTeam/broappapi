import Koa from 'koa'
// import serve from 'koa-static'
// import path from 'path'
// import * as http from 'http'
import * as https from 'https'
import fs from 'fs'
import socket from 'socket.io'
// import socketJWT from 'socketio-jwt';
import session from 'koa-session'
import koaqs from 'koa-qs'
import mongoose from 'mongoose'
import cors from '@koa/cors'
import respond from 'koa-respond'
import bodyParser from 'koa-bodyparser'
import compress from 'koa-compress'
import { scopePerRequest, loadControllers } from 'awilix-koa'

import { logger } from './logger'
import { configureContainer } from './container'
import { notFoundHandler } from '../middleware/not-found'
import { errorHandler } from '../middleware/error-handler'
import { registerContext } from '../middleware/register-context'

import '../utils/auth'
import { env } from './env'

import passport from 'koa-passport'

// SSL options
var options = {
  key: fs.readFileSync('./localhost.key'),
  cert: fs.readFileSync('./localhost.crt')
}

/**
 * Creates and returns a new Koa application.
 * Does *NOT* call `listen`!
 *
 * @return {Promise<http.Server>} The configured app.
 */
export async function createServer() {
  logger.debug('Creating server...')

  mongoose.connect(env.MONGO_URL || 'mongodb://127.0.0.1:27017/bro-app')

  const app = new Koa()

  koaqs(app)

  app.keys = ['super-secret-key']

  // Container is configured with our services and whatnot.
  const container = (app.container = configureContainer())

  app
    // .use(serve(path.join(__dirname, '/../../public')))
    // Top middleware is the error handler.
    .use(errorHandler)
    .use(session({}, app))
    // Compress all responses.
    .use(compress())
    // Adds ctx.ok(), ctx.notFound(), etc..
    .use(respond())
    // Handles CORS.
    .use(cors({ credentials: true }))
    // Parses request bodies.
    .use(bodyParser())
    // Creates an Awilix scope per request. Check out the awilix-koa
    // docs for details: https://github.com/jeffijoe/awilix-koa
    .use(scopePerRequest(container))
    // Create a middleware to add request-specific data to the scope.
    .use(registerContext)
    /* passport */
    .use(passport.initialize())
    /* persistent login sessions */
    .use(passport.session())
    // Load routes (API "controllers")
    .use(loadControllers('../routes/*.js', { cwd: __dirname }))
    // Default handler when nothing stopped the chain.
    .use(notFoundHandler)

  // Creates a http server ready to listen.
  // const server = http.createServer(app.callback())
  const server = https.createServer(options, app.callback())

  app.io = socket(server, {})

  /* app.io.on('connection', socketJwt.authorize({
    secret: env.JWT_TOKEN || 'mysecretkey',
    timeout: 15000
  })).on('authenticated', socket => {
    console.log('this is the name from the JWT: ' + socket.decoded_token.displayName);
  }); */

  app.io.on('connection', sock => {
    // console.log(sock);
  })

  // Add a `close` event listener so we can clean up resources.
  server.on('close', () => {
    // You should tear down database connections, TCP connections, etc
    // here to make sure Jest's watch-mode some process management
    // tool does not release resources.
    logger.debug('Server closing, bye!')
  })

  logger.debug('Server created, ready to listen', { scope: 'startup' })
  return server
}
