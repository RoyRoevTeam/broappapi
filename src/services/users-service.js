import { NotFound, BadRequest } from 'fejl'
import User from '../models/User'

const assertId = BadRequest.makeAssert('No id given')

/**
 * Users Service.
 * Gets a users.
 */
export default class UsersService {
  // find() {
  //   return User.find()
  // }

  get(id) {
    return User
      .findById(id)
  }

  // create(data) {
  //   BadRequest.assert(data, 'No todo payload given')
  //   BadRequest.assert(data.title, 'title is required')
  //   BadRequest.assert(data.title.length < 100, 'title is too long')
  //   return User.create(data)
  // }

  // async update(id, data) {
  //   assertId(id)
  //   BadRequest.assert(data, 'No todo payload given')
  //
  //   // Make sure the todo exists by calling `get`.
  //   await this.get(id)
  //
  //   // Prevent overposting.
  //   // const picked = pickProps(data);
  //   // return Event.update(id, picked)
  // }

  // async remove(id) {
  //   // Make sure the todo exists by calling `get`.
  //   await this.get(id)
  //   return User.remove(id)
  // }
}
