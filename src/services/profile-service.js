import passport from 'koa-passport'

import User from '../models/User'

/**
 * Auth Service.
 */
export default class AuthService {
  async getProfile(ctx, next) {
    await passport.authenticate('jwt', { session: false })
    if (!ctx.request.body.email || !ctx.request.body.password) {
      ctx.status = 400
      ctx.body = {
        error:
          'expected an object with username, password, email, name but got: ' +
          ctx.request.body
      }
      return
    }

    ctx.body = await User.create(ctx.request.body)
    ctx.status = 200

    next()
  }
}
