import { NotFound, BadRequest } from 'fejl'
import Event from '../models/Event'

const assertId = BadRequest.makeAssert('No id given')

/**
 * Events Service.
 * Gets a events.
 */
export default class EventsService {
  find() {
    return Event.find()
  }

  async get(id) {
    assertId(id)
    // If `Event.get()` returns a falsy value, we throw a
    // NotFound error with the specified message.

    return Event.findOne({ _id: id }).then(
      NotFound.makeAssert(`Todo with id "${id}" not found`)
    )
  }

  create(data) {
    BadRequest.assert(data, 'No todo payload given')
    BadRequest.assert(data.title, 'title is required')
    BadRequest.assert(data.title.length < 100, 'title is too long')
    return Event.create(data)
  }

  async update(id, data) {
    assertId(id)
    BadRequest.assert(data, 'No todo payload given')

    // Make sure the todo exists by calling `get`.
    await this.get(id)

    // Prevent overposting.
    // const picked = pickProps(data);
    // return Event.update(id, picked)
  }

  async remove(id) {
    // Make sure the todo exists by calling `get`.
    await this.get(id)
    return Event.remove(id)
  }
}
