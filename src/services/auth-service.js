import { BadRequest } from 'fejl'
import passport from 'koa-passport'

import User from '../models/User'

/**
 * Auth Service.
 */
export default class AuthService {
  async register(ctx, next) {
    if (!ctx.request.body.email || !ctx.request.body.password) {
      ctx.status = 400
      ctx.body = {
        error:
          'expected an object with username, password, email, name but got: ' +
          ctx.request.body
      }
    }

    ctx.body = await User.create(ctx.request.body)
    ctx.status = 200
  }

  async login(ctx, next) {
    await passport.authenticate('local', (err, user) => {
      if (err) {
        console.log(err.stack)
      }

      BadRequest.assert(user, 'Не верный логин или пароль')

      ctx.login(user, err => {
        if (err) {
          console.log(err.stack)
        }

        const payload = {
          id: user.id,
          displayName: user.displayName,
          email: user.email
        }

        ctx.body = payload
      })
    })(ctx, next)
  }

  async getUser(ctx) {
    const user = ctx.state.user

    const payload = {
      id: user.id,
      displayName: user.displayName,
      email: user.email
    }

    ctx.body = payload

    ctx.app.io.in(ctx.session.socketId).emit('authenticated', payload)
  }
}
