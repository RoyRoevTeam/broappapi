import LocalStrategy from 'passport-local'
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt'
import { Strategy as GoogleStrategy } from 'passport-google-auth'
import { Strategy as FacebookStrategy } from 'passport-facebook'
import { Strategy as VKStrategy } from 'passport-vkontakte'
import passport from 'koa-passport'

import { env } from '../lib/env'

import User from '../models/User'

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: env.JWT_TOKEN || 'mysecretkey'
}

const cbFunction = async (token, tokenSecret, profile, done) => {
  // Retrieve user from database, if exists
  let email = profile.emails[0].value
  const user = await User.findOne({ email })
  if (user) {
    done(null, user)
  } else {
    // If user not exist, create it
    const newUser = {
      firstName: profile.name.givenName,
      lastName: profile.name.familyName,
      password: 'password-is-from-google',
      email
    }
    const createdUser = await User.create(newUser)
    if (createdUser) {
      done(null, createdUser)
    } else {
      done(null, false)
    }
  }
}

passport.serializeUser((user, done) => {
  done(null, user)
})

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user)
  })
})

passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      session: false
    },
    (email, password, done) => {
      User.findOne({ email }, (err, user) => {
        if (err) {
          return done(err)
        }

        if (!user || !user.checkPassword(password)) {
          return done(null, false, {
            message: 'User does not exist or wrong password.'
          })
        }
        return done(null, user)
      })
    }
  )
)

passport.use(
  new JwtStrategy(jwtOptions, function(payload, done) {
    User.findById(payload.id, (err, user) => {
      if (err) {
        return done(err)
      }
      if (user) {
        done(null, user)
      } else {
        done(null, false)
      }
    })
  })
)

/**
 * google strategy of Passport.js
 *
 * @param
 * @returns
 */

passport.use(
  new GoogleStrategy(
    {
      clientId: env.GOOGLE_CLIENT_ID,
      clientSecret: env.GOOGLE_CLIENT_SECRET,
      callbackURL: `${env.HOST}/auth/google/callback`
    },
    cbFunction
  )
)

/**
 * Facebook strategy of Passport.js
 *
 * @param
 * @returns
 */

passport.use(
  new FacebookStrategy(
    {
      clientID: env.FACEBOOK_CLIENT_ID,
      clientSecret: env.FACEBOOK_CLIENT_SECRET,
      callbackURL: `${env.HOST}/auth/facebook/callback`,
      profileFields: ['id', 'displayName', 'name', 'photos', 'email']
    },
    cbFunction
  )
)

passport.use(
  new VKStrategy(
    {
      clientID: env.VK_CLIENT_ID,
      clientSecret: env.VK_CLIENT_SECRET,
      callbackURL: `${env.HOST}/auth/vk/callback`,
      profileFields: ['id', 'displayName', 'name', 'photos', 'email'],
      apiVersion: '5.32'
    },
    async (token, tokenSecret, params, profile, done) => {
      const email = params.email
      const user = await User.findOne({ email })
      if (user) {
        done(null, user)
      } else {
        // If user not exist, create it
        const newUser = {
          firstName: profile.name.givenName,
          lastName: profile.name.familyName,
          password: 'password-is-from-google',
          email
        }
        const createdUser = await User.create(newUser)
        if (createdUser) {
          done(null, createdUser)
        } else {
          done(null, false)
        }
      }
    }
  )
)
