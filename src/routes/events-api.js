import { createController } from 'awilix-koa'
import { authenticated } from '../utils'

const api = eventsService => ({
  find: async (ctx, next) => ctx.ok(await eventsService.find(ctx, next)),
  create: async ctx => ctx.ok(await eventsService.create(ctx.request.body)),
  getEvent: async ctx => ctx.ok(await eventsService.get(ctx.params.id))
})

export default createController(api)
  .prefix('/events')
  .get('/:id', 'getEvent')
  .get('/', 'find')
  .post('/', 'create', {
    before: [authenticated()]
  })
