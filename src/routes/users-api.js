import { createController } from 'awilix-koa'

const api = usersService => ({
  getUser: async (ctx, next) => ctx.ok(await usersService.get(ctx.params.id, next)
    .then(userData => ({
      // map user data from DB
      email: userData.email
    })))
})

export default createController(api)
  .prefix('/users')
  .get('/:id', 'getUser')
