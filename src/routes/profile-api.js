import { createController } from 'awilix-koa'

const api = profileService => ({
  getProfile: async (ctx, next) =>
    ctx.ok(await profileService.getProfile(ctx, next))
})

export default createController(api)
  .prefix('/profile')
  .get('/', 'getProfile')
