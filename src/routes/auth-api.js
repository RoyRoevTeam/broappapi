import { createController } from 'awilix-koa'
import passport from 'koa-passport'
import { authenticated, saveSocketId } from '../utils'

const api = authService => ({
  register: async (ctx, next) => ctx.ok(await authService.register(ctx, next)),
  login: async (ctx, next) => ctx.ok(await authService.login(ctx, next)),
  google: passport.authenticate('google'),
  googleCallback: passport.authenticate('google', {
    successRedirect: '/auth/authenticated',
    failureRedirect: '/'
  }),
  facebook: passport.authenticate('facebook'),
  facebookCallback: passport.authenticate('facebook', {
    successRedirect: '/auth/authenticated',
    failureRedirect: '/'
  }),
  vk: passport.authenticate('vkontakte', {
    scope: ['status', 'email', 'friends', 'notify'],
    display: 'mobile'
  }),
  vkCallback: passport.authenticate('vkontakte', {
    successRedirect: '/auth/authenticated',
    failureRedirect: '/',
    scope: ['status', 'email', 'friends', 'notify']
  }),
  authenticated: async ctx => ctx.ok(await authService.getUser(ctx))
})

export default createController(api)
  .prefix('/auth')
  .before([saveSocketId()])
  .post('/register', 'register')
  .post('/login', 'login')
  .get('/google', 'google')
  .get('/google/callback', 'googleCallback')
  .get('/facebook', 'facebook')
  .get('/facebook/callback', 'facebookCallback')
  .get('/vk', 'vk')
  .get('/vk/callback', 'vkCallback')
  .get('/authenticated', 'authenticated', {
    before: [authenticated()]
  })
